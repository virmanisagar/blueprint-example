from flask import Flask, render_template
from admin.second import second1

app = Flask(__name__)
app.register_blueprint(second1, url_prefix="/admin")


@app.route("/")
def test():
    return "<h1>test</h1>"
    

if __name__ == "__main__":
    app.run(debug=True)
