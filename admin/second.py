from flask import Blueprint, render_template

second1 = Blueprint("second", __name__, template_folder="templates")

@second1.route("/home")
@second1.route("/")
def home():
    return render_template("home.html")
    

@second1.route("/test")
def test():
    return "<h1>TEST1 second route</h1>"
